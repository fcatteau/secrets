package utils

// TODO: rename to a meaningful name

import (
	"fmt"
	"io"

	"github.com/logrusorgru/aurora"
)

// WithWarning runs the function passed as argument and prints a warning if it returns an error
func WithWarning(writer io.Writer, warning string, fun func() error) {
	err := fun()
	if err != nil {
		fmt.Fprintln(writer, aurora.Sprintf(
			aurora.Brown("Warning: %s (%s)\n"),
			warning,
			err.Error()))
	}
}
