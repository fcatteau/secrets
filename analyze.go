package main

import (
	"fmt"
	"os"
	"sort"

	"github.com/logrusorgru/aurora"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/git"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/scanner/gitleaks"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/scanner/trufflehog"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/utils"
)

// analyze runs the tools and produces a report containing issues for each detected secret leak.
func analyze(c *cli.Context, path string) (*issue.Report, error) {
	// First, place the files in a new git repository, in one single commit.
	// This is needed as the tools search the commit history for secret leaks and
	// searching into the full history would find leaks that don't exist anymore.
	flatPath, err := git.FlattenRepo(path)
	if err != nil {
		// TODO: extract function that logs errors in a consistent way: func logErr(msg string, err error)
		fmt.Fprintln(c.App.Writer, aurora.Sprintf(aurora.Red("Couldn't prepare the repository for analysis: %v"), err))
		return nil, err
	}
	defer utils.WithWarning(
		c.App.Writer,
		fmt.Sprintf("couldn't remove temporary directory %s", flatPath),
		func() error { return os.RemoveAll(flatPath) })

	// Run the tools.
	// TODO: refactor to avoid passing *cli.Context to Run functions.
	gitleaksIssues, err := gitleaks.Run(c, flatPath)
	if err != nil {
		fmt.Fprintln(c.App.Writer, aurora.Sprintf(aurora.Red("Gitleaks analysis failed: %v"), err))
		return nil, err
	}
	truffleHogIssues, err := trufflehog.Run(c, flatPath)
	if err != nil {
		fmt.Fprintln(c.App.Writer, aurora.Sprintf(aurora.Red("TruffleHog analysis failed: %v"), err))
		return nil, err
	}

	// Merge Gitleaks and TruffleHog issues
	var issues []issue.Issue = append(gitleaksIssues, truffleHogIssues...)

	// Deduplicate Gitleaks and TruffleHog issues, remove entropy issues contained in others and consolidate
	// them into a single issue if present on several consecutive lines
	issues = consolidateEntropyIssues(cleanEntropyIssues(deduplicate(issues)))

	sort.Sort(ByName(issues))

	// Return the report
	if issues == nil {
		// We need to initialize the slice for correct JSON marshalling
		issues = []issue.Issue{}
	}
	report := issue.NewReport()
	report.Vulnerabilities = issues
	return &report, nil
}
