package convert

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestCompareKey(t *testing.T) {
	want := "main.go:dec0b7eaaa1d5d97b39bf32c9080624c17f0e0ec03445b440898e7fe8dc61b07:AWS"
	got := CompareKey("main.go", "dec0b7eaaa1d5d97b39bf32c9080624c17f0e0ec03445b440898e7fe8dc61b07", "AWS")
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestLocation(t *testing.T) {
	want := issue.Location{
		File:      "main.go",
		LineStart: 14,
		LineEnd:   14,
	}
	got := Location("main.go", 14, 14)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestFingerprint(t *testing.T) {
	want := "dec0b7eaaa1d5d97b39bf32c9080624c17f0e0ec03445b440898e7fe8dc61b07"
	got := Fingerprint("AWS_KEY = '12345';")
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestIdentifier(t *testing.T) {
	want := issue.Identifier{
		Type:  "gitleaks_rule_id",
		Name:  "Gitleaks rule ID AWS",
		Value: "AWS",
	}
	got := Identifier("Gitleaks", "AWS")
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
