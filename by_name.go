package main

import (
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

// ByName type to sort issues by Name, File, Line and Tool.
type ByName []issue.Issue

func (a ByName) Len() int {
	return len(a)
}

func (a ByName) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a ByName) Less(i, j int) bool {
	switch strings.Compare(a[i].Name, a[j].Name) {
	case -1:
		return true
	case 1:
		return false
	}
	switch strings.Compare(a[i].Location.File, a[j].Location.File) {
	case -1:
		return true
	case 1:
		return false
	}
	if a[i].Location.LineStart < a[j].Location.LineStart {
		return true
	}
	if a[i].Location.LineStart > a[j].Location.LineStart {
		return false
	}
	return strings.Compare(a[i].Scanner.ID, a[j].Scanner.ID) == -1
}
