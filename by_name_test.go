package main

import (
	"reflect"
	"sort"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

var aws = issue.Issue{
	Name:        "AWS key",
	Description: "AWS key detected, please check.",
	Location: issue.Location{
		File:      "main.go",
		LineStart: 14,
		LineEnd:   14,
	},
	Scanner: issue.Scanner{
		ID: "Gitleaks",
	},
}

var rsa = issue.Issue{
	Name:        "RSA key",
	Description: "RSA key file detected, please check.",
	Location: issue.Location{
		File:      "id_rsa",
		LineStart: 1,
		LineEnd:   31,
	},
	Scanner: issue.Scanner{
		ID: "Gitleaks",
	},
}

func TestByName(t *testing.T) {
	var issues = []issue.Issue{rsa, aws}
	var sorted = []issue.Issue{aws, rsa}
	sort.Sort(ByName(issues))
	if !reflect.DeepEqual(issues, sorted) {
		t.Errorf("Wrong result. Expected a sorted slice, but got:\n%#v", issues)
	}
}
