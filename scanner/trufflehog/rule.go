package trufflehog

import "gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/rule"

// See https://github.com/dxa4481/truffleHogRegexes/blob/master/truffleHogRegexes/regexes.json for a list of
var rules = map[string]rule.Rule{
	"AWS API Key":     rule.InfoAWS,
	"Facebook Oauth":  rule.InfoFacebook,
	"Generic API Key": rule.InfoGenericAPI,
	"Generic Secret": {
		Name:        "Generic secret",
		Description: rule.SingleCredDesc("Generic secret"),
	},
	"GitHub": rule.InfoGitHub,
	"Google (GCP) Service-account": {
		Name:        "Google GCP service account",
		Description: rule.SingleCredDesc("Google GCP service account"),
	},
	"Google Oauth": {
		Name:        "Google OAuth token",
		Description: rule.SingleCredDesc("Google OAuth token"),
	},
	"Heroku API Key": {
		Name:        "Heroku API key",
		Description: rule.SingleCredDesc("Heroku API key"),
	},
	"PGP private key block": rule.InfoPGP,
	"Password in URL": {
		Name:        "Password in URL",
		Description: rule.SingleCredDesc("Password in URL"),
	},
	"RSA private key": rule.InfoRSA,
	"SSH (DSA) private key": {
		Name:        "SSH DSA private key",
		Description: rule.SingleCredDesc("SSH DSA private key"),
	},
	"SSH (EC) private key": {
		Name:        "SSH EC private key",
		Description: rule.SingleCredDesc("SSH EC private key"),
	},
	"SSH (OPENSSH) private key": rule.InfoSSH,
	"Slack Token":               rule.InfoSlack,
	"Slack Webhook": {
		Name:        "Slack Webhook",
		Description: rule.SingleCredDesc("Slack Webhook"),
	},
	"Twilio API Key": {
		Name:        "Twilio API key",
		Description: rule.SingleCredDesc("Twilio API key"),
	},
	"Twitter Oauth": rule.InfoTwitter,
}
