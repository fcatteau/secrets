module gitlab.com/gitlab-org/security-products/analyzers/secrets/v2

require (
	bou.ke/monkey v1.0.1 // indirect
	github.com/logrusorgru/aurora v0.0.0-20181002194514-a7b3b318ed4e
	github.com/mitchellh/copystructure v1.0.0
	github.com/otiai10/copy v0.0.0-20180813032824-7e9a647135a1
	github.com/otiai10/curr v0.0.0-20150429015615-9b4961190c95 // indirect
	github.com/otiai10/mint v1.2.3 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.1.6
	golang.org/x/crypto v0.0.0-20190320223903-b7391e95e576 // indirect
	golang.org/x/net v0.0.0-20190322120337-addf6b3196f6 // indirect
	golang.org/x/sys v0.0.0-20190322080309-f49334f85ddc // indirect
	gopkg.in/src-d/go-git.v4 v4.10.0
)
