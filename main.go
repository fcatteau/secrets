// The analyzer program finds leaks of secrets like token and cryptographic keys in a directory.
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/scanner/gitleaks"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/utils"
)

const (
	flagTargetDir   = "target-dir"
	flagArtifactDir = "artifact-dir"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "Secrets analyzer for GitLab SAST"
	app.Author = "GitLab"
	app.Commands = []cli.Command{runCommand()}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func runCommand() cli.Command {
	flags := []cli.Flag{
		cli.StringFlag{
			Name:   flagTargetDir,
			Usage:  "Target directory",
			EnvVar: command.EnvVarTargetDir + "," + command.EnvVarCIProjectDir,
		},
		cli.StringFlag{
			Name:   flagArtifactDir,
			Usage:  "Artifact directory",
			EnvVar: command.EnvVarArtifactDir + "," + command.EnvVarCIProjectDir,
		},
	}
	flags = append(flags, gitleaks.MakeFlags()...)

	return cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a compatible artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			// no args
			if c.Args().Present() {
				if err := cli.ShowSubcommandHelp(c); err != nil {
					return err
				}
				return errors.New("invalid number of arguments")
			}

			// target directory
			targetDir, err := filepath.Abs(c.String(flagTargetDir))
			if err != nil {
				return err
			}

			report, err := analyze(c, targetDir)
			if err != nil {
				return err
			}

			// write indented JSON to artifact
			artifactPath := filepath.Join(c.String(flagArtifactDir), command.ArtifactNameSAST)
			f, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer utils.WithWarning(c.App.Writer, fmt.Sprintf("couldn't close file %s", artifactPath), f.Close)
			enc := json.NewEncoder(f)
			enc.SetIndent("", "  ")
			return enc.Encode(report)
		},
	}
}
