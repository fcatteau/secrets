package trufflehog

import (
	"log"
	"os"
	"path/filepath"
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestToIssues(t *testing.T) {
	in := `{"branch": "origin/master", "commit": "i\n", "commitHash": "5ec1e27c70c81b5cc060eed22166790bd59286f1", "date": "2019-02-15 14:38:12", "diff": "@@ -0,0 +1,15 @@\n+-----BEGIN RSA PRIVATE KEY-----\n+MIICXgIBAAKBgQDsI2RKhCKoe3PbMS5ymwJSzodX5OWQxTZsShoZUI73dKhh/UWF\n+Wr49nBYfC0BNo+AAK9SbOgqzGAc1E55bGCOBEV95grmiw+XnzuoxkFjkGqYaPnPj\n+ct0KAveHNvCp5PJnj4S/wbHBMhv9PvKuBQrCUiEtt1qE2ZDodmwQ719H2wIDAQAB\n+AoGBAIwNubAQitXO0rpQgzHqCgi/Abr5wcaJkWvMz4nwI7tU74GezBr55Rd5eg1T\n+iQIDjhnL5T9jv3DVcYDjb+qreHIzBLX8u6qFgP2jrm7OUPwfFrEn8XLJ+kOntzgo\n+ovscffUCFdsZE23ynRFSAv74//vWNP2JwXGrjYd+iq+Y35cxAkEA+OFuk/XTj0eA\n+LGn9mq+gUH4xz39lpYJ2N8Ou4mxePu2sZWp2GNxuiIvCl3gpdXXx3i1kDJGcHOUR\n+gsPAHClNxwJBAPLkpZcRNrDtXTYRRwPFGDdBULncYbJYE7iwoBKwL8egbcPNaFFr\n+cE//4Fouu2+X1p3BUFw4SHp/9NodlgwiBU0CQEjPgs8smuEO9POJapUnjkoeMSpY\n+Rf9+xGEaEX7SX1wfolDlLXmme3vdD2fK0q43fDQYqW135+kYJuBPrlNOzh8CQQC7\n+wJhrfMfc3a5Wk+Zj+J9uXtOqx7rRJBoigo1JxN06Hz2ZxBamETmg7TacH1GNwj4I\n+dZzLnEoSPwSi8YnB2S5lAkEAo4/t5CijvopsiKpHe46ADxQLd6fOr/LYQiZYiGCH\n+KUE24MIW/SngRcJZChMEz0ffU2w083ZWd5fGbmU7Ycv3zA==\n+-----END RSA PRIVATE KEY-----\n", "path": "testdata/id_rsa", "printDiff": "\u001b[93m-----BEGIN RSA PRIVATE KEY-----\u001b[0m", "reason": "RSA private key", "stringsFound": ["-----BEGIN RSA PRIVATE KEY-----"]}
{"branch": "origin/master", "commit": "i\n", "commitHash": "5ec1e27c70c81b5cc060eed22166790bd59286f1", "date": "2019-02-15 14:38:12", "diff": "@@ -0,0 +1,61 @@\n+package main\n+\n+import (\n+        \"log\"\n+        \"os\"\n+)\n+\n+const EMPTYSTR string = \"\"\n+const AUTH = \"ES_AUTH:2a5:retbe\"\n+const AWS_ID = \"AKIAIOSFODNN7EXAMPLE\"\n+const AWS_KEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"\n+const WORDPRESS = \"XahV8uKN^/MvXC` + "`" + `4U+ZI!Q$0XvomTTHAM-[Wu|hg1B[P+%-8$y-%W35qW2hW9 HZ\"\n+const HEROKU_KEY = \"12345678-ABCD-ABCD-ABCD-1234567890ABC\"\n+const SSHKEY = ` + "`" + `-----BEGIN RSA PRIVATE KEY-----\n+Proc-Type: 4,ENCRYPTED\n+DEK-Info: DES-EDE3-CBC,86C3F4011519BFBB\n+\n+PxyzMAlAmEu/Qkx9nPh696SU7/MjXpCpOnfFiijLhJumNcRlWgsOiI9rfwlkh4aN\n++MeuMV7ciXLd+rmPfwimnr5XydeuJ6PkvQok+GvooohL6ktEP2zlNCsdTOMRV5eK\n+Piy0M+alMund8M9urUrxTXv1GqhBWBAnC/LAzEsa3lHP9S87sRLNs+NSOcyu5w2g\n+y5D9sqU/q4yz9+B64BiVRSkDvXzBXo/OsEecPLe/y7itUISdgyhBUra0XfCdnNxG\n+DjQQbZwgqM0DtDlVHs87pIFN2UUEXCGkYGIYenh03IHeZEHhlh2Qp57nMQ+0lLYZ\n+qej96rUJBjJwUVIoKgu1pPRhGDoNyeXgzML44FZ3XnP7xfX3AJZ+CjInRalE16Ou\n+Tzd6aXGFIz1xHuWRDQ8qRBid3/rLtMOTNCav38RBawlqimXs8/3/oggVzhCSzxFf\n+gr/CFd5vQAxmZlchIDv3ECL6+vSB0T8mZYTZfK41SAiIRHdCfcdC/amqylGlSohU\n+BcYZ7/wTYw65/ThoPt35plbLOvzo1cInL6VhHqFJwGljmcyZVQKFF5rOmxIJgS5K\n+RMWJUM8fYxggluKTpaPbcO2Wr1NdcbhbktOjkpS2WtDlrzb55/8NoeZnSLFHlJ6B\n+D6lotk0JK9dQ9CgYe5+J6oB4G1mzmaHel3xW7KjYb69dD+egdiZGBy/Ma7Y9DV/b\n+ZHdFVGQe+53aRxFYcC5xG+u9L7nrxsAp7lFPOb7ZjeosTtqing6dJGyLUdONVwc9\n+TVwPMcg0EvKWknZkCspwusk5UQ3iYyDMNz+u1+7hphXmpcUH0wpzu76t1k8uOTzA\n+hbtP3X0vublerJaKv7MOO9JefaiTDf2ow+eT1X7kRtn8yhPo1CZnrVTZ+jrbxY5D\n+sLogR2zNsim4FtUNCjuSVNAJ6m9sRNmamGJlkXMYUOa2ToA0aiRsYNwqqWnphS5t\n+4F7P58AatDLO9DW/XtWrCW3425CuNHEczYhbkE9eAqEPrbh27nLCh6e49T3YJBWy\n+vfwEBDjqDZEdIcR0Pz2covmeful8VffPobriGXLJUq/+Zu2TLQHFWiXzJFhFS98W\n+Pymv7klxiv/JlhxzGu1sLI9yRV294rluSAHAaaTEJm6P+5FNp2A1V5epBqMnlFcr\n+OJyslN5StFeuHhFoYqBu4aqtG09YPJhtJv1FeVQBzxy2q04f0V4sX+HOT1R0eXZf\n+7cBiabv6SjnXlXCP8THPGD4HNB0nkhb+wK7lmRuSsuFOxjj3bvolQH15Wq83njxf\n+eKlZH9lYHIzoR/O2v5B8d7IikJKQLhpGBBzsWIy5e80hC4Pzya5cvdZDulL4636w\n+WxchPMCRd/3VHPY0YyZ097ZT8Ny+KMo1+ZEK0KNT1YL28QRTyJl13uqKAA6EMpEI\n+ZTF1v92z0sVCkjyvaMijtNwtWpNG2hRAnw4L5I98QUahakaPU5L3g4HOyXsSLuiO\n+gHB/8VTKp8AmhYzQKOm7Pt0xVrZFHZb/wNzNKeloOGjndhKoH/OigDvWivKijcTK\n+qU42DgWp65ahpcTjdBpAWvFXz60toSj4QDwaxTX+qdW5B2JXKoKuQQ==\n+-----END RSA PRIVATE KEY-----\n+` + "`" + `\n+\n+var opts *Options\n+\n+\n+func main() {\n+        args := os.Args[1:]\n+\n+        db, err := setupDB()\n+        if err != nil {\n+                log.Fatal(err)\n+        }\n+        defer db.Close()\n+\n+        opts = parseOptions(args)\n+        serverStart(opts, db)\n+}\n+\n", "path": "testdata/main.go", "printDiff": "\u001b[93mAKIAIOSFODNN7EXAMPLE\u001b[0m", "reason": "AWS API Key", "stringsFound": ["AKIAIOSFODNN7EXAMPLE"]}
{"branch": "origin/master", "commit": "i\n", "commitHash": "5ec1e27c70c81b5cc060eed22166790bd59286f1", "date": "2019-02-15 14:38:12", "diff": "@@ -0,0 +1,61 @@\n+package main\n+\n+import (\n+        \"log\"\n+        \"os\"\n+)\n+\n+const EMPTYSTR string = \"\"\n+const AUTH = \"ES_AUTH:2a5:retbe\"\n+const AWS_ID = \"AKIAIOSFODNN7EXAMPLE\"\n+const AWS_KEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"\n+const WORDPRESS = \"XahV8uKN^/MvXC` + "`" + `4U+ZI!Q$0XvomTTHAM-[Wu|hg1B[P+%-8$y-%W35qW2hW9 HZ\"\n+const HEROKU_KEY = \"12345678-ABCD-ABCD-ABCD-1234567890ABC\"\n+const SSHKEY = ` + "`" + `-----BEGIN RSA PRIVATE KEY-----\n+Proc-Type: 4,ENCRYPTED\n+DEK-Info: DES-EDE3-CBC,86C3F4011519BFBB\n+\n+PxyzMAlAmEu/Qkx9nPh696SU7/MjXpCpOnfFiijLhJumNcRlWgsOiI9rfwlkh4aN\n++MeuMV7ciXLd+rmPfwimnr5XydeuJ6PkvQok+GvooohL6ktEP2zlNCsdTOMRV5eK\n+Piy0M+alMund8M9urUrxTXv1GqhBWBAnC/LAzEsa3lHP9S87sRLNs+NSOcyu5w2g\n+y5D9sqU/q4yz9+B64BiVRSkDvXzBXo/OsEecPLe/y7itUISdgyhBUra0XfCdnNxG\n+DjQQbZwgqM0DtDlVHs87pIFN2UUEXCGkYGIYenh03IHeZEHhlh2Qp57nMQ+0lLYZ\n+qej96rUJBjJwUVIoKgu1pPRhGDoNyeXgzML44FZ3XnP7xfX3AJZ+CjInRalE16Ou\n+Tzd6aXGFIz1xHuWRDQ8qRBid3/rLtMOTNCav38RBawlqimXs8/3/oggVzhCSzxFf\n+gr/CFd5vQAxmZlchIDv3ECL6+vSB0T8mZYTZfK41SAiIRHdCfcdC/amqylGlSohU\n+BcYZ7/wTYw65/ThoPt35plbLOvzo1cInL6VhHqFJwGljmcyZVQKFF5rOmxIJgS5K\n+RMWJUM8fYxggluKTpaPbcO2Wr1NdcbhbktOjkpS2WtDlrzb55/8NoeZnSLFHlJ6B\n+D6lotk0JK9dQ9CgYe5+J6oB4G1mzmaHel3xW7KjYb69dD+egdiZGBy/Ma7Y9DV/b\n+ZHdFVGQe+53aRxFYcC5xG+u9L7nrxsAp7lFPOb7ZjeosTtqing6dJGyLUdONVwc9\n+TVwPMcg0EvKWknZkCspwusk5UQ3iYyDMNz+u1+7hphXmpcUH0wpzu76t1k8uOTzA\n+hbtP3X0vublerJaKv7MOO9JefaiTDf2ow+eT1X7kRtn8yhPo1CZnrVTZ+jrbxY5D\n+sLogR2zNsim4FtUNCjuSVNAJ6m9sRNmamGJlkXMYUOa2ToA0aiRsYNwqqWnphS5t\n+4F7P58AatDLO9DW/XtWrCW3425CuNHEczYhbkE9eAqEPrbh27nLCh6e49T3YJBWy\n+vfwEBDjqDZEdIcR0Pz2covmeful8VffPobriGXLJUq/+Zu2TLQHFWiXzJFhFS98W\n+Pymv7klxiv/JlhxzGu1sLI9yRV294rluSAHAaaTEJm6P+5FNp2A1V5epBqMnlFcr\n+OJyslN5StFeuHhFoYqBu4aqtG09YPJhtJv1FeVQBzxy2q04f0V4sX+HOT1R0eXZf\n+7cBiabv6SjnXlXCP8THPGD4HNB0nkhb+wK7lmRuSsuFOxjj3bvolQH15Wq83njxf\n+eKlZH9lYHIzoR/O2v5B8d7IikJKQLhpGBBzsWIy5e80hC4Pzya5cvdZDulL4636w\n+WxchPMCRd/3VHPY0YyZ097ZT8Ny+KMo1+ZEK0KNT1YL28QRTyJl13uqKAA6EMpEI\n+ZTF1v92z0sVCkjyvaMijtNwtWpNG2hRAnw4L5I98QUahakaPU5L3g4HOyXsSLuiO\n+gHB/8VTKp8AmhYzQKOm7Pt0xVrZFHZb/wNzNKeloOGjndhKoH/OigDvWivKijcTK\n+qU42DgWp65ahpcTjdBpAWvFXz60toSj4QDwaxTX+qdW5B2JXKoKuQQ==\n+-----END RSA PRIVATE KEY-----\n+` + "`" + `\n+\n+var opts *Options\n+\n+\n+func main() {\n+        args := os.Args[1:]\n+\n+        db, err := setupDB()\n+        if err != nil {\n+                log.Fatal(err)\n+        }\n+        defer db.Close()\n+\n+        opts = parseOptions(args)\n+        serverStart(opts, db)\n+}\n+\n", "path": "testdata/main.go", "printDiff": "\u001b[93m-----BEGIN RSA PRIVATE KEY-----\u001b[0m", "reason": "RSA private key", "stringsFound": ["-----BEGIN RSA PRIVATE KEY-----"]}
{"branch": "origin/master", "commit": "i\n", "commitHash": "5ec1e27c70c81b5cc060eed22166790bd59286f1", "date": "2019-02-15 14:38:12", "diff": "@@ -0,0 +1,61 @@\n+package main\n+\n+import (\n+        \"log\"\n+        \"os\"\n+)\n+\n+const EMPTYSTR string = \"\"\n+const AUTH = \"ES_AUTH:2a5:retbe\"\n+const AWS_ID = \"AKIAIOSFODNN7EXAMPLE\"\n+const AWS_KEY = \"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY\"\n+const WORDPRESS = \"XahV8uKN^/MvXC` + "`" + `4U+ZI!Q$0XvomTTHAM-[Wu|hg1B[P+%-8$y-%W35qW2hW9 HZ\"\n+const HEROKU_KEY = \"12345678-ABCD-ABCD-ABCD-1234567890ABC\"\n+const SSHKEY = ` + "`" + `-----BEGIN RSA PRIVATE KEY-----\n+Proc-Type: 4,ENCRYPTED\n+DEK-Info: DES-EDE3-CBC,86C3F4011519BFBB\n+\n+PxyzMAlAmEu/Qkx9nPh696SU7/MjXpCpOnfFiijLhJumNcRlWgsOiI9rfwlkh4aN\n++MeuMV7ciXLd+rmPfwimnr5XydeuJ6PkvQok+GvooohL6ktEP2zlNCsdTOMRV5eK\n+Piy0M+alMund8M9urUrxTXv1GqhBWBAnC/LAzEsa3lHP9S87sRLNs+NSOcyu5w2g\n+y5D9sqU/q4yz9+B64BiVRSkDvXzBXo/OsEecPLe/y7itUISdgyhBUra0XfCdnNxG\n+DjQQbZwgqM0DtDlVHs87pIFN2UUEXCGkYGIYenh03IHeZEHhlh2Qp57nMQ+0lLYZ\n+qej96rUJBjJwUVIoKgu1pPRhGDoNyeXgzML44FZ3XnP7xfX3AJZ+CjInRalE16Ou\n+Tzd6aXGFIz1xHuWRDQ8qRBid3/rLtMOTNCav38RBawlqimXs8/3/oggVzhCSzxFf\n+gr/CFd5vQAxmZlchIDv3ECL6+vSB0T8mZYTZfK41SAiIRHdCfcdC/amqylGlSohU\n+BcYZ7/wTYw65/ThoPt35plbLOvzo1cInL6VhHqFJwGljmcyZVQKFF5rOmxIJgS5K\n+RMWJUM8fYxggluKTpaPbcO2Wr1NdcbhbktOjkpS2WtDlrzb55/8NoeZnSLFHlJ6B\n+D6lotk0JK9dQ9CgYe5+J6oB4G1mzmaHel3xW7KjYb69dD+egdiZGBy/Ma7Y9DV/b\n+ZHdFVGQe+53aRxFYcC5xG+u9L7nrxsAp7lFPOb7ZjeosTtqing6dJGyLUdONVwc9\n+TVwPMcg0EvKWknZkCspwusk5UQ3iYyDMNz+u1+7hphXmpcUH0wpzu76t1k8uOTzA\n+hbtP3X0vublerJaKv7MOO9JefaiTDf2ow+eT1X7kRtn8yhPo1CZnrVTZ+jrbxY5D\n+sLogR2zNsim4FtUNCjuSVNAJ6m9sRNmamGJlkXMYUOa2ToA0aiRsYNwqqWnphS5t\n+4F7P58AatDLO9DW/XtWrCW3425CuNHEczYhbkE9eAqEPrbh27nLCh6e49T3YJBWy\n+vfwEBDjqDZEdIcR0Pz2covmeful8VffPobriGXLJUq/+Zu2TLQHFWiXzJFhFS98W\n+Pymv7klxiv/JlhxzGu1sLI9yRV294rluSAHAaaTEJm6P+5FNp2A1V5epBqMnlFcr\n+OJyslN5StFeuHhFoYqBu4aqtG09YPJhtJv1FeVQBzxy2q04f0V4sX+HOT1R0eXZf\n+7cBiabv6SjnXlXCP8THPGD4HNB0nkhb+wK7lmRuSsuFOxjj3bvolQH15Wq83njxf\n+eKlZH9lYHIzoR/O2v5B8d7IikJKQLhpGBBzsWIy5e80hC4Pzya5cvdZDulL4636w\n+WxchPMCRd/3VHPY0YyZ097ZT8Ny+KMo1+ZEK0KNT1YL28QRTyJl13uqKAA6EMpEI\n+ZTF1v92z0sVCkjyvaMijtNwtWpNG2hRAnw4L5I98QUahakaPU5L3g4HOyXsSLuiO\n+gHB/8VTKp8AmhYzQKOm7Pt0xVrZFHZb/wNzNKeloOGjndhKoH/OigDvWivKijcTK\n+qU42DgWp65ahpcTjdBpAWvFXz60toSj4QDwaxTX+qdW5B2JXKoKuQQ==\n+-----END RSA PRIVATE KEY-----\n+` + "`" + `\n+\n+var opts *Options\n+\n+\n+func main() {\n+        args := os.Args[1:]\n+\n+        db, err := setupDB()\n+        if err != nil {\n+                log.Fatal(err)\n+        }\n+        defer db.Close()\n+\n+        opts = parseOptions(args)\n+        serverStart(opts, db)\n+}\n+\n", "path": "testdata/main.go", "printDiff": "\u001b[93mHEROKU_KEY = \"12345678-ABCD-ABCD-ABCD-1234567890AB\u001b[0m", "reason": "Heroku API Key", "stringsFound": ["HEROKU_KEY = \"12345678-ABCD-ABCD-ABCD-1234567890AB"]}`
	want := []issue.Issue{
		{
			Category:    "sast",
			Name:        "RSA private key",
			Message:     "RSA private key",
			Description: "RSA private key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/id_rsa:8bcac7908eb950419537b91e19adc83ce2c9cbfdacf4f81157fdadfec11f7017:RSA private key",
			Severity:    7,
			Confidence:  2,
			Solution:    "",
			Scanner: issue.Scanner{
				ID:   "trufflehog",
				Name: "TruffleHog",
			},
			Location: issue.Location{
				File:      "testdata/id_rsa",
				LineStart: 1,
				LineEnd:   15,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "trufflehog_rule_id",
					Name:  "TruffleHog rule ID RSA private key",
					Value: "RSA private key",
				},
			},
		},
		{
			Category:    "sast",
			Name:        "AWS API key",
			Message:     "AWS API key",
			Description: "Amazon Web Services API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:1a5d44a2dca19669d72edf4c4f1c27c4c1ca4b4408fbb17f6ce4ad452d78ddb3:AWS API Key",
			Severity:    7,
			Confidence:  2,
			Scanner: issue.Scanner{
				ID:   "trufflehog",
				Name: "TruffleHog",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 7,
				LineEnd:   7,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "trufflehog_rule_id",
					Name:  "TruffleHog rule ID AWS API Key",
					Value: "AWS API Key",
				},
			},
		},
		{
			Category:    "sast",
			Name:        "RSA private key",
			Message:     "RSA private key",
			Description: "RSA private key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:8bcac7908eb950419537b91e19adc83ce2c9cbfdacf4f81157fdadfec11f7017:RSA private key",
			Severity:    7,
			Confidence:  2,
			Scanner: issue.Scanner{
				ID:   "trufflehog",
				Name: "TruffleHog",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 11,
				LineEnd:   40,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "trufflehog_rule_id",
					Name:  "TruffleHog rule ID RSA private key",
					Value: "RSA private key",
				},
			},
		},
		{
			Category:    "sast",
			Name:        "Heroku API key",
			Message:     "Heroku API key",
			Description: "Heroku API key detected; please remove and revoke it if this is a leak.",
			CompareKey:  "testdata/main.go:54767793881cfed5d45a1d28b318679f6f930fc32e84236200f56dcfd98d8b73:Heroku API Key",
			Severity:    7,
			Confidence:  2,
			Scanner: issue.Scanner{
				ID:   "trufflehog",
				Name: "TruffleHog",
			},
			Location: issue.Location{
				File:      "testdata/main.go",
				LineStart: 10,
				LineEnd:   10,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "trufflehog_rule_id",
					Name:  "TruffleHog rule ID Heroku API Key",
					Value: "Heroku API Key",
				},
			},
		},
	}
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	b := []byte(in)
	got, err := toIssues(os.Stderr, filepath.Join(dir, "..", "..", "test", "fixtures"), b)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
