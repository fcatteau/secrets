package gitleaks

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"syscall"

	"github.com/logrusorgru/aurora"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v2/utils"
)

const (
	flagEntropyLevel   = "gitleaks-entropy-level"
	pathGitleaks       = "gitleaks"
	pathGitleaksConfig = "/gitleaks.toml"
)

var PemBegin, PemEnd *regexp.Regexp

var issueScanner = issue.Scanner{
	ID:   "gitleaks",
	Name: "Gitleaks",
}

type Secret struct {
	Line     string `json:"line"`
	Offender string `json:"offender"`
	Reason   string `json:"reason"`
	File     string `json:"file"`
}

// MakeFlags returns the cli flags.
func MakeFlags() []cli.Flag {
	return []cli.Flag{
		cli.Float64Flag{
			Name:   flagEntropyLevel,
			Usage:  "Gitleaks entropy level (0.0 to 8.0)",
			EnvVar: "SAST_GITLEAKS_ENTROPY_LEVEL",
			Value:  8.0,
		},
	}
}

// Run runs Gitleaks on the path and transforms results into issues.
func Run(c *cli.Context, path string) ([]issue.Issue, error) {

	// Extract cli options
	var level = fmt.Sprintf("%f", c.Float64(flagEntropyLevel))

	// Create a temporary file. Gitleaks can't output to stdout.
	tmpFile, err := ioutil.TempFile("", "gitleaks-*.json")
	if err != nil {
		fmt.Fprintln(c.App.Writer, aurora.Sprintf(
			aurora.Red("Couldn't create temporary file: %v"),
			err))
		return nil, err
	}
	defer utils.WithWarning(c.App.Writer, fmt.Sprintf("couldn't close temporary file %s", tmpFile.Name()), tmpFile.Close)

	// Run Gitleaks on the given path.
	cmd := exec.Command(pathGitleaks, "-e", level, "--report", tmpFile.Name(), "-r", path, "--config", pathGitleaksConfig)
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr
	err = cmd.Run()

	// Gitleaks exits with these status codes:
	// 0: no leaks
	// 1: leaks present
	// 2: error encountered
	if err == nil {
		return []issue.Issue{}, nil // no leaks
	}
	if exitErr, ok := err.(*exec.ExitError); ok {
		if exitErr.Sys().(syscall.WaitStatus).ExitStatus() == 1 {
			return toIssues(c.App.Writer, path, tmpFile) // leaks present
		}
	}
	fmt.Fprintln(c.App.Writer, aurora.Sprintf(aurora.Red("Couldn't run the gitleaks command: %v"), err))
	return nil, err
}

// toIssues converts a Gitleaks report into issues while adding line number.
func toIssues(w io.Writer, path string, reportFile io.Reader) ([]issue.Issue, error) {
	var secrets []Secret

	// Decode JSON report
	err := json.NewDecoder(reportFile).Decode(&secrets)
	if err != nil {
		fmt.Fprintln(w, aurora.Sprintf(aurora.Red("Couldn't parse the Gitleaks report: %v"), err))
		return nil, err
	}

	// Translate to issues
	var issues []issue.Issue
	for _, secret := range secrets {
		sourceCode := secret.Line
		filePath := secret.File

		// Open the file possibly containing a secret, to compute the line number.
		f, err := os.Open(filepath.Join(path, filePath))
		if err != nil {
			fmt.Println(w, aurora.Sprintf(aurora.Red("Couldn't open source file %s: %v"), filePath, err))
			return nil, err
		}

		// Search for the source code extract in each line of the file. Also detect end line for the PEM format.
		// TODO: extract function
		reader := bufio.NewReader(f)
		line := 1
		lineStart := 0
		lineEnd := 0
		isPem := PemBegin.MatchString(sourceCode)

		for {
			text, err := reader.ReadString('\n')

			if err == io.EOF {
				break
			} else if err != nil {
				fmt.Fprintln(w, aurora.Sprintf(aurora.Red("Problem while reading source file %s: %v"), filePath, err))
				return nil, err
			}

			if strings.Contains(text, sourceCode) {
				// Found the line
				lineStart = line
				lineEnd = line
				if !isPem {
					// No more work to do, this is not a multiline PEM secret.
					break
				}
			} else if PemEnd.MatchString(text) && isPem && lineStart != 0 {
				// End of the PEM block.
				lineEnd = line
				break
			}
			line++
		}

		if err = f.Close(); err != nil {
			fmt.Fprintln(w, aurora.Sprintf(aurora.Red("Problem while closing source file %s: %v"), filePath, err))
			return nil, err
		}

		// Compute Name, Description and RuleID.
		// TODO: extract function
		var name, description, ruleID string
		rule, ruleFound := rules[secret.Reason]
		switch {
		case ruleFound:
			name = rule.Name
			description = rule.Description
			ruleID = secret.Reason
		case strings.HasPrefix(secret.Reason, "Entropy: "):
			// A string with high entropy has been detected.
			name = "High entropy string"
			description = "A string with high entropy was found, this could be a secret"
			ruleID = "Entropy"
		default:
			// This is an unknown rule. Warn the user and use default values.
			fmt.Fprintln(os.Stderr,
				aurora.Sprintf(
					aurora.Red("No description for Gitleaks rule %s, please open an issue on https://gitlab.com/gitlab-org/gitlab-ee/issues"),
					secret.Reason))

			name = fmt.Sprint("Gitleaks rule ", secret.Reason)
			description = fmt.Sprint("Gitleaks rule ", secret.Reason, " detected a secret")
			ruleID = secret.Reason
		}

		// append the issue.
		issues = append(issues, issue.Issue{
			Category:    issue.CategorySast,
			Scanner:     issueScanner,
			Name:        name,
			Message:     name,
			Description: description,
			CompareKey:  convert.CompareKey(filePath, convert.Fingerprint(sourceCode), ruleID),
			Severity:    issue.LevelCritical,
			Confidence:  issue.LevelUnknown,
			Location:    convert.Location(filePath, lineStart, lineEnd),
			Identifiers: convert.Identifiers("Gitleaks", ruleID),
		})
	}

	return issues, nil
}

func init() {
	PemBegin = regexp.MustCompile("-----BEGIN")
	PemEnd = regexp.MustCompile("-----END")
}
